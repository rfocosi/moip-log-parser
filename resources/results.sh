#!/bin/sh
echo "Top 3 most called"
egrep -o 'request_to="[^"]+"' log.txt | sort | uniq -c | sort -nr | head -n3
# OR
# sort log.txt -k3 | awk '{ print $3 }' | uniq -c | sort -nr | head -n3

echo "\nRequests by response_status"
egrep -o 'response_status="[0-9]+"' log.txt | sort | uniq -c
