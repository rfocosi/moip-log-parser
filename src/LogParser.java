import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class LogParser {
	public static void main(String[] args) {
		String fileName = args[0];

		topRequested(fileName, 3);

		System.out.println();

		statusCodes(fileName);
	}

	private static void topRequested(String fileName, int limit) {
		System.out.println("Top "+limit+" callers");
		System.out.println("Requests,URL");

		try {
			Map<Integer, String> fields = groupAndCountFields(fileName, "request_to");

			int count = 0;
			for (Entry<Integer,String> entry : fields.entrySet()) {
				if (++count > limit) break;
				System.out.println(entry.getKey() +","+ entry.getValue());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void statusCodes(String fileName) {
		System.out.println("Requests by response_status");
		System.out.println("Requests,Status Code");

		try {
			Map<Integer,String> fields = groupAndCountFields(fileName, "response_status");

			for (Entry<Integer,String> entry : fields.entrySet()) {
				System.out.println(entry.getKey() +","+ entry.getValue());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static SortedMap<Integer,String> groupAndCountFields(String fileName, String fieldName) throws IOException {

		Map<String, Integer> fieldCount = new HashMap<>();
		SortedMap<Integer, String> orderedFieldsByHits = new TreeMap<>(Comparator.reverseOrder());

		Pattern pattern = Pattern.compile(".*"+ fieldName + "=\"([^\"]+)\".*", Pattern.CASE_INSENSITIVE);

		try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			for(String line; (line = br.readLine()) != null; ) {
				Matcher m = pattern.matcher(line);
				if (m.matches()) {
					String fieldValue = m.group(1);
					fieldCount.put(fieldValue, fieldCount.getOrDefault(fieldValue, 0) + 1);
				}
			}
		}

		for (Entry<String, Integer> entry : fieldCount.entrySet()) {
			orderedFieldsByHits.put(entry.getValue(), entry.getKey());
		}

		return orderedFieldsByHits;
	}

}
